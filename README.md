# House Price Prediction

The aim of this project is to predict house prices using self-created regression analysis method. This project make use of already available libaries Seaborn, NumPy, Pandas, and Matplotlib. The dataset is available at: https://www.kaggle.com/datasets/dgomonov/new-york-city-airbnb-open-data

